\documentclass[12pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper,margin=1in}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amsmath, amsthm,amssymb}

\title{REFLECTION AND TRANSMISSION COEFFICIENTS}
\author{Huy Le}
%\date{}							% Activate to display a given date or no date

\begin{document}
\maketitle
Consider a homogeneous isotropic linear elastic solid, whose constitutive equations between stresses, $\sigma_{ij}$, and strains, $\epsilon_{ij}$, are given through the Lam\'{e}'s constants, $\lambda$ and $\mu$, as following:
\begin{equation}
\sigma_{ij}=\lambda \delta_{ij} \epsilon_{kk}  +2\mu\epsilon_{ij},
\label{eq:constitutive}
\end{equation}
where $\delta_{ij}$ is the Kronecker's delta.
\par
With $\rho$ defining density and $\mathbf{f}$ defining the seismic source, the wave equations in the concerned solid are expressed in terms of displacements, $\mathbf{u}$, as:
\begin{equation}
\rho \ddot{\mathbf{u}} = (\lambda+2\mu)\nabla(\nabla \cdot \mathbf{u}) - \mu \nabla \times (\nabla \times \mathbf{u}) + \mathbf{f}.
\label{eq:wave}
\end{equation}
The strains are defined from displacements as:
\begin{equation}
\epsilon_{ij}=\frac{1}{2}\left( \frac{\partial u_i}{\partial j}+\frac{\partial u_j}{\partial i}\right)
\end{equation}
\par
By Lam\'{e}'s theorem, solutions of the above wave equations can be written in terms of Helmholtz potentials, $\phi$ and $\boldsymbol{\psi}$, as:
\begin{equation}
\mathbf{u}=\nabla\phi+\nabla\times\boldsymbol{\psi},
\label{eq:helmholtz}
\end{equation}
where these potentials satisfy:
\begin{subequations}
\begin{gather}
\ddot{\phi}=\alpha^2 \nabla^2\phi + \frac{\Phi}{\rho}, \label{eq:pwave}\\
\ddot{\boldsymbol{\psi}}=\beta^2 \nabla^2\boldsymbol{\psi} + \frac{\boldsymbol{\Psi}}{\rho}, \label{eq:swave}\\
\nabla \cdot \boldsymbol{\psi} = 0, \label{eq:cond}
\end{gather}
\end{subequations}
with $\alpha^2=\frac{\lambda+2\mu}{\rho}$, $\beta^2=\frac{\mu}{\rho}$, and $\Phi$ and $\boldsymbol{\Psi}$ being the potentials of the source obtained by the decomposition, $\mathbf{f}=\nabla\Phi + \nabla \times \boldsymbol{\Psi}$. $\nabla\phi$ and $\nabla\times\boldsymbol{\psi}$ define the P-wave and S-wave components of $\mathbf{u}$, with velocities $\alpha$ and $\beta$ respectively. 
\par
Equation \ref{eq:cond} is of very importance. The wave equations \ref{eq:wave} have three unknowns: the three components of the displacement field, $\mathbf{u}$. Helmholtz decomposition \ref{eq:helmholtz}, however, introduces four unknowns: one scalar potential, $\phi$, and three components of a vector potential, $\boldsymbol{\psi}$. Equation \ref{eq:cond} balances the number of unknowns and the number of equations.
\par
Separation of variables gives solutions of the homogeneous wave equations as superpositions of plane waves of the forms:
\begin{subequations}
\begin{gather}
\phi=Ae^{i\omega\left(\frac{\hat{\mathbf{k}}}{\alpha}\cdot\mathbf{x}-t\right)}, \\
\boldsymbol{\psi}=\mathbf{A}e^{i\omega\left(\frac{\hat{\mathbf{k}}}{\beta}\cdot\mathbf{x}-t\right)},
\end{gather}
\end{subequations}
where $\omega$ is the angular frequency and $\hat{\mathbf{k}}$ is the unit wave vector signifying the direction of propagation. As a result, the P- and S-components of the displacements are:
\begin{subequations}
\begin{gather}
\mathbf{u}^P=\nabla\phi=i\omega\frac{\hat{\mathbf{k}}}{\alpha}Ae^{i\omega\left(\frac{\hat{\mathbf{k}}}{\alpha}\cdot\mathbf{x}-t\right)}, \\
\mathbf{u}^S=\nabla\times\boldsymbol{\psi}=i\omega\frac{\hat{\mathbf{k}}}{\beta} \times \mathbf{A} e^{i\omega\left(\frac{\hat{\mathbf{k}}}{\beta}\cdot\mathbf{x}-t\right)},
\end{gather}
\end{subequations}
which show that the P-wave component is polarized in the direction of $\hat{\mathbf{k}}$, parallel to the propagation direction, while the S-wave component is polarized in the direction of $\hat{\mathbf{k}} \times \mathbf{A}$, perpendicular to the propagation direction. Note that $\frac{\hat{\mathbf{k}}}{\alpha}$ and $\frac{\hat{\mathbf{k}}}{\beta}$ are defined as P- and S-slowness vectors, $\mathbf{s}^P$ and $\mathbf{s}^S$, respectively.
\par
When a plane P-wave propagates in a 2D xz-plane, $\phi=\phi(x,z,t)$, its displacement is given by:
\begin{equation}
\mathbf{u}^P=\left( \frac{\partial\phi}{\partial x},0,\frac{\partial\phi}{\partial z}\right).
\end{equation}
\par
When a plane S-wave propagates in a 2D xz-plane, $\boldsymbol{\psi}=\boldsymbol{\psi}(x,z,t)$, equation \ref{eq:cond} gives:
\begin{equation}
\frac{\partial \psi_x}{\partial x}+\frac{\partial \psi_z}{\partial z}=0,
\end{equation}
which together with the fact that the y-component of the shear displacement is zero,
\begin{equation}
\frac{\partial \psi_x}{\partial z}-\frac{\partial \psi_z}{\partial x}=0,
\end{equation}
forms Cauchy-Riemann equations. Physically, $\psi_x$ and $\psi_z$ are bounded and differentiable, so the function $\psi_z+i\psi_x$ is a bounded analytic function of $x+iz$. From Liouville's theorem, this function is constant. Since the displacements only concern derivatives of $\psi_x$ and $\psi_z$, the constant can be taken as zero, meaning $\psi_x=\psi_z=0$. Consequently, for a 2D propagation, the vector potential has only one non-zero component: 
\begin{equation}
\boldsymbol{\psi}=(0,\psi,0). 
\end{equation}
The S-displacements are given by:
\begin{equation}
\mathbf{u}^S=\left(-\frac{\partial\psi}{\partial z},0,\frac{\partial\psi}{\partial x}\right).
\end{equation}
\par
In order to derive the reflection and transmission coefficients, consider two half-spaces with boundary defined by $z=0$. An incident P-wave, $P$, in the first half-space generates P- and S-reflected waves, $P_1$ and $S_1$, in the first half-space and P- and S-transmitted waves, $P_2$ and $S_2$, in the second half-space (Figure \ref{fig:fig1}). Define $\theta$'s the angles these waves form with the interface's normal and the potentials $\phi$, $\phi_1$, $\psi_1$, $\phi_2$, and $\psi_2$, for the incident $P$, reflected $P_1$, reflected $S_1$, transmitted $P_2$, and transmitted $S_2$ waves respectively, functions of $(x,z,t)$:
\begin{subequations}
\begin{align}
\phi&=e^{i\omega\left(\mathbf{s}^P\cdot\mathbf{x}-t\right)}, \\
\phi_1&=Ae^{i\omega\left(\mathbf{s}^{P_1}\cdot\mathbf{x}-t\right)}, \\
\psi_1&=Be^{i\omega\left(\mathbf{s}^{S_1}\cdot\mathbf{x}-t\right)}, \\
\phi_2&=Ce^{i\omega\left(\mathbf{s}^{P_2}\cdot\mathbf{x}-t\right)}, \\
\psi_2&=De^{i\omega\left(\mathbf{s}^{S_2}\cdot\mathbf{x}-t\right)},
\end{align}
\end{subequations} 
with the corresponding slowness vectors:
\begin{subequations}
\begin{align}
\mathbf{s}^P&=\frac{1}{\alpha_1}(\sin{\theta^P},0,-\cos{\theta^P}), \\
\mathbf{s}^{P_1}&=\frac{1}{\alpha_1}(\sin{\theta^{P_1}},0,\cos{\theta^{P_1}}), \\
\mathbf{s}^{S_1}&=\frac{1}{\beta_1}(\sin{\theta^{S_1}},0,\cos{\theta^{S_1}}), \\
\mathbf{s}^{P_2}&=\frac{1}{\alpha_2}(\sin{\theta^{P_2}},0,-\cos{\theta^{P_2}}), \\
\mathbf{s}^{S_2}&=\frac{1}{\beta_2}(\sin{\theta^{S_2}},0,-\cos{\theta^{S_2}}).
\end{align}
\label{eq:slownesses}
\end{subequations} 
\par
Their displacements are respectively:
\begin{subequations}
\begin{align}
\mathbf{u}^P&=\left( \frac{\partial\phi}{\partial x},0,\frac{\partial\phi}{\partial z}\right)=i\omega(s^P_x,0,s^P_z)\phi,\\
\mathbf{u}^{P_1}&=\left( \frac{\partial\phi_1}{\partial x},0,\frac{\partial\phi_1}{\partial z}\right)=i\omega(s^{P_1}_x,0,s^{P_1}_z)\phi_1,\\
\mathbf{u}^{S_1}&=\left(-\frac{\partial\psi_1}{\partial z},0,\frac{\partial\psi_1}{\partial x}\right)=i\omega(-s^{S_1}_z,0,s^{S_1}_x)\psi_1,\\
\mathbf{u}^{P_2}&=\left( \frac{\partial\phi_2}{\partial x},0,\frac{\partial\phi_2}{\partial z}\right)=i\omega(s^{P_2}_x,0,s^{P_2}_z)\phi_2,\\
\mathbf{u}^{S_2}&=\left(-\frac{\partial\psi_2}{\partial z},0,\frac{\partial\psi_2}{\partial x}\right)=i\omega(-s^{S_2}_z,0,s^{S_2}_x)\psi_2.
\end{align}
\label{eq:displacements}
\end{subequations} 
The combined displacements in the two half-spaces are:
\begin{subequations} 
\begin{gather}
\mathbf{u}_1=\mathbf{u}^P+\mathbf{u}^{P_1}+\mathbf{u}^{S_1},\\
\mathbf{u}_2=\mathbf{u}^{P_2}+\mathbf{u}^{S_2}.
\end{gather}
\label{eq:displacements12}
\end{subequations}
\par
Suppose the two half-spaces are solids in welded contact, where no slip occurs. Boundary conditions impose that the displacements and tractions are continuous at the interface. From equations \ref{eq:displacements} and \ref{eq:displacements12}, components of the displacement fields at the interface, $z=0$, involve terms like $e^{i\omega\left(\frac{\sin{\theta^P}}{\alpha_1}x-t\right)}$ for the incident $P$ waves and those similar for the reflected $P_1$, reflected $S_1$, transmitted $P_2$, and transmitted $S_2$ waves. Equating these terms everywhere on the boundary implies the familiar Snell's law:
\begin{equation}
\frac{\sin{\theta^P}}{\alpha_1}=\frac{\sin{\theta^{P_1}}}{\alpha_1}=\frac{\sin{\theta^{S_1}}}{\beta_1}=\frac{\sin{\theta^{P_2}}}{\alpha_2}=\frac{\sin{\theta^{S_2}}}{\beta_2}=p,
\label{eq:snell}
\end{equation}
where $p$ is the newly defined ray parameter. In terms of slownesses, Snell's law states that horizontal slownesses do not change across the boundary: $s^P_x=s^{P_1}_x=s^{S_1}_x=s^{P_2}_x=s^{S_2}_x=p$. As a result, continuity of the displacements at the boundary imply:
\begin{subequations}
\begin{gather}
p(1+A)-s^{S_1}_zB=pC-s^{S_2}_zD,\\
s^P_z(1-A)+pB=s^{P_2}_zC+pD.
\end{gather}
\label{eq:coeffs1}
\end{subequations}
\par
With the interface's normal, $\hat{\mathbf{n}}=(0,0,1)$, tractions involve only the last column of the stress tensor, $\mathbf{T}=(\sigma_{xz},\sigma_{yz},\sigma_{zz})$. Specifically, for the considered waves:
\begin{subequations}
\begin{align}
\mathbf{T}^P&=\left( 2\mu_1\frac{\partial^2\phi}{\partial x\partial z},0,\lambda_1\nabla^2\phi+2\mu_1\frac{\partial^2\phi}{\partial z^2}\right)=-\omega^2\left(2\mu_1ps^P_z,0,\frac{\lambda_1}{\alpha_1^2}+2\mu_1(s^P_z)^2\right)\phi,\\
\mathbf{T}^{P_1}&=\left( 2\mu_1\frac{\partial^2\phi_1}{\partial x\partial z},0,\lambda_1\nabla^2\phi_1+2\mu_1\frac{\partial^2\phi_1}{\partial z^2}\right)=-\omega^2\left(2\mu_1ps^{P_1}_z,0,\frac{\lambda_1}{\alpha_1^2}+2\mu_1(s^{P_1}_z)^2\right)\phi_1,\\
\mathbf{T}^{S_1}&=\left(\mu_1\left(\frac{\partial^2\psi_1}{\partial x^2}-\frac{\partial^2\psi_1}{\partial z^2}\right),0,2\mu_1\frac{\partial^2\psi_1}{\partial x\partial z}\right)=-\omega^2\left(\mu_1\left[p^2-(s^{S_1}_z)^2\right],0,2\mu_1ps^{S_1}_z\right)\psi_1,\\
\mathbf{T}^{P_2}&=\left( 2\mu_2\frac{\partial^2\phi_2}{\partial x\partial z},0,\lambda_2\nabla^2\phi_2+2\mu_2\frac{\partial^2\phi_2}{\partial z^2}\right)=-\omega^2\left(2\mu_2ps^{P_2}_z,0,\frac{\lambda_2}{\alpha_2^2}+2\mu_2(s^{P_2}_z)^2\right)\phi_2,\\
\mathbf{T}^{S_2}&=\left(\mu_2\left(\frac{\partial^2\psi_2}{\partial x^2}-\frac{\partial^2\psi_2}{\partial z^2}\right),0,2\mu_2\frac{\partial^2\psi_2}{\partial x\partial z}\right)=-\omega^2\left(\mu_2\left[p^2-(s^{S_2}_z)^2\right],0,2\mu_2ps^{S_2}_z\right)\psi_2.
\end{align}
\label{eq:tractions}
\end{subequations} 
The combined tractions in the two half-spaces are:
\begin{subequations} 
\begin{gather}
\mathbf{T}_1=\mathbf{T}^P+\mathbf{T}^{P_1}+\mathbf{T}^{S_1},\\
\mathbf{T}_2=\mathbf{T}^{P_2}+\mathbf{T}^{S_2}.
\end{gather}
\label{eq:tractions12}
\end{subequations}
Continuity of the tractions at the boundary imply:
\begin{subequations}
\begin{gather}
2\mu_1ps^P_z(1-A)+\mu_1\left[p^2-(s^{S_1}_z)^2\right]B=2\mu_2ps^{P_2}_zC+\mu_2\left[p^2-(s^{S_2}_z)^2\right]D,\\
\left[\frac{\lambda_1}{\alpha_1^2}+2\mu_1(s^P_z)^2\right](1+A)+2\mu_1ps^{S_1}_zB=\left[\frac{\lambda_2}{\alpha_2^2}+2\mu_2(s^{P_2}_z)^2\right]C+2\mu_2ps^{S_2}_zD.
\end{gather}
\label{eq:coeffs2}
\end{subequations}
\par
Equations \ref{eq:coeffs1} and \ref{eq:coeffs2} form a system of four equations for four coefficients $A$, $B$, $C$, and $D$. Here are considered two special cases: a free surface and a fluid/fluid contact. For a free surface, $\lambda_2=\mu_2=0$ and we drop the subscript of the first half-space. There are reflected P-wave, $P_1$, and reflected S-wave, $S_1$.The only boundary conditions are those of tractions, equations \ref{eq:coeffs2}. In this case, the tractions vanish on at the free surface:
\begin{subequations}
\begin{gather}
2p\frac{\cos{\theta^P}}{\alpha} (1-A)+\left(\frac{1}{\beta^2}-2p^2\right)B=0,\\
(1-2\beta^2p^2)(1+A)+2\beta p\cos{\theta^{S_1}}B=0.
\end{gather}
\end{subequations}
The reflection and transmission coefficients are solved:
\begin{subequations}
\begin{align}
A&=\frac{4\beta^3p^2\cos{\theta^P}\cos{\theta^{S_1}}-\alpha(1-2\beta^2p^2)}{4\beta^3p^2\cos{\theta^P}\cos{\theta^{S_1}}+\alpha(1-2\beta^2p^2)},\\
B&=\frac{-4\beta^2p\cos{\theta^P}(1-2\beta^2p^2)}{4\beta^3p^2\cos{\theta^P}\cos{\theta^{S_1}}+\alpha(1-2\beta^2p^2)}.
\end{align}
\end{subequations}
\par
For the acoustic case where two fluids are in contact, there are P-reflected, $P_1$, and P-transmitted, $P_2$, waves and only the normal components of the displacements and tractions are continuous:
\begin{subequations}
\begin{gather}
\frac{\cos{\theta^P}}{\alpha_1}(1-A)=\frac{\cos{\theta^{P_2}}}{\alpha_2}C,\\
\rho_1(1+A)=\rho_2C,
\end{gather} 
\end{subequations}
which give:
\begin{subequations}
\begin{align}
A&=-\frac{\alpha_1\rho_1\cos{\theta^{P_2}}-\alpha_2\rho_2\cos{\theta^P}}{\alpha_1\rho_1\cos{\theta^{P_2}}+\alpha_2\rho_2\cos{\theta^P}},\\
B&=\frac{2\alpha_2\rho_1\cos{\theta^P}}{\alpha_1\rho_1\cos{\theta^{P_2}}+\alpha_2\rho_2\cos{\theta^P}}.
\end{align}
\end{subequations}

\begin{figure}
\includegraphics[width=\textwidth]{fig1}
\caption{The reflections and transmission of an incident P-wave.}
\label{fig:fig1}
\end{figure}


\end{document}  